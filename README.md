# Git Lab Version Manager

This app helps to improve GitLab CI/CD and add possibility to manage revision TAGS
based on [GitFlow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow) branching strategy

All information about versioning you can find [here](https://semver.org/) and [here](https://git-scm.com/book/en/v2/Git-Basics-Tagging)

![trigers!](/md_ressources/trigger_schema.png)

## Before start

You must create 4 values for your repository

<ol>
  <li>MAJOR = 0</li>
  <li>MINOR = 0</li>
  <li>HOTFIX = 0</li>
  <li>PRIVATE_TOKEN (Your gitlab api token)</li>
</ol>

WARNING!: Be careful to set your PRIVATE_TOKEN as Protected and Masked

![values!](/md_ressources/values.png)

## Installation

You can include direclty ci job [.version.ci.yml](.version.ci.yml) 
to your .gitlab-ci.yml
## Or use this template

```yaml
version_up:
  stage: versioning
  image: registry.gitlab.com/prysvi_projects/python/gitlab-version-manager:latest
  script:
    - "cd /app"
    - $RUN_COMMAND
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      when: never
    - if: '$CI_COMMIT_REF_NAME != $CI_COMMIT_BRANCH'
      when: never
    - if: $CI_COMMIT_TITLE =~ /Merge branch (\'hotfix\/.*?\') into 'release'/
      variables:
        RUN_COMMAND: 'python3 main.py --action hotfix'
    - if: $CI_COMMIT_TITLE == "Merge branch 'develop' into 'release'"
      variables:
        RUN_COMMAND: 'python3 main.py --action minor'
    - if: $CI_COMMIT_TITLE == "Merge branch 'release' into 'main'"
      variables:
        RUN_COMMAND: 'python3 main.py --action tag'
    - if: $CI_COMMIT_TAG && $CI_COMMIT_TAG =~ /^(?P<major>0|[1-9]\d*)\.(?P<minor>0|[1-9]\d*)\.(?P<patch>0|[1-9]\d*)(?:-(?P<prerelease>(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\+(?P<buildmetadata>[0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?$/
      variables:
        RUN_COMMAND: 'python3 main.py --action reset'
```

##SUFFIX in your version
if you want to make the version with suffix (**alpha**) like 1.0.0-**alpha**. 


You mast to add custom tag to merge request (**release**->**main**) title. Example: SUFFIX:**alpha**:

![](/md_ressources/sufifx.png)


## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.
