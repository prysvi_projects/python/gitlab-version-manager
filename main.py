import argparse
import logging
import os
import re

import requests

logging.basicConfig()
logging.getLogger().setLevel(logging.INFO)


class VersionController(object):
    def __init__(self):
        # CI VARS
        self.api_url: str = os.environ.get('CI_API_V4_URL', None)
        self.project_id: str = os.environ.get('CI_PROJECT_ID', None)
        self.private_token = os.environ.get('PRIVATE_TOKEN', None)
        for k, v in self.__dict__.items():
            if v is None:
                raise ValueError(f" value {k} is not set")
        self.headers = {'PRIVATE-TOKEN': self.private_token}
        # VERSION VARS
        self._major: int = int(os.environ.get('MAJOR', 0))
        self._minor: int = int(os.environ.get('MINOR', 0))
        self._hotfix: int = int(os.environ.get('HOTFIX', 0))

    def get_variable_url(self, var_name: str) -> str:
        return f"{self.api_url}/projects/{self.project_id}/variables/{var_name}"

    def set_version(self, major: int, minor: int, hotfix: int):
        self._major = major
        self._minor = minor
        self._hotfix = hotfix
        logging.info(f"set_version {self._major}.{self._minor}.{self._hotfix}")
        requests.request('put', self.get_variable_url("MAJOR"), data={'value': major}, headers=self.headers)
        requests.request('put', self.get_variable_url("MINOR"), data={'value': minor}, headers=self.headers)
        requests.request('put', self.get_variable_url("HOTFIX"), data={'value': hotfix}, headers=self.headers)

    def tag_branch(self):
        commit: str = os.environ.get('CI_COMMIT_MESSAGE', None)
        commit_sha: str = os.environ.get('CI_COMMIT_SHA', None)

        if commit and 'SUFFIX:' in commit:
            suffix: str = re.search("SUFFIX:(.*):", commit).group(1)
            url = f"{self.api_url}/projects/{self.project_id}/repository/tags?tag_name=" \
                  f"{self._major}.{self._minor}.{self._hotfix}-{suffix}&ref={commit_sha}"
            logging.info(f"found tag version suffix {suffix}")
        else:
            url = f"{self.api_url}/projects/{self.project_id}/repository/tags?tag_name=" \
                  f"{self._major}.{self._minor}.{self._hotfix}&ref={commit_sha}"

        logging.info(f"it's release!!!  {self._major}.{self._minor}.{self._hotfix}")
        requests.request('post', url, headers=self.headers)
        logging.info("Reset hotfix after release")

    def minor(self):
        self.set_version(major=self._major, minor=self._minor + 1, hotfix=0)

    def hotfix(self):
        self.set_version(major=self._major, minor=self._minor, hotfix=self._hotfix + 1)

    def tag(self):
        self.tag_branch()
        self.set_version(major=self._major, minor=self._minor, hotfix=0)

    def reset(self):
        self.set_version(major=self._major, minor=self._minor, hotfix=0)

if __name__ == "__main__":
    logging.info("Running Version Controller....")
    parser = argparse.ArgumentParser(description='Version Manager Information')
    parser.add_argument('--action', dest='action', type=str, help='Action to do')
    parser.add_argument('--suffix', dest='suffix', type=str, help='suffix to add')
    args = parser.parse_args()
    logging.info(f"It's {args.action} version")
    if not args.action:
        logging.error("--action key required [hotfix,minor,tag,reset] allowed")
        exit(2)
    version_controller: VersionController = VersionController()
    eval(f"version_controller.{args.action}()")
