import os
from unittest import mock, TestCase
from unittest.mock import MagicMock, call

import requests

from main import VersionController


class TestVersionController(TestCase):

    @classmethod
    def setUpClass(cls):
        os.environ.setdefault('CI_API_V4_URL', "http://local.git.home/v4")
        os.environ.setdefault('CI_PROJECT_ID', "99")
        os.environ.setdefault('PRIVATE_TOKEN', "token")
        requests.request = MagicMock()  # avoid external call

    @mock.patch('requests.request')
    def test_set_version(self, spy_mock: MagicMock):
        version_controller: VersionController = VersionController()
        version_controller.set_version(1, 2, 3)
        headers = {'PRIVATE-TOKEN': os.environ.get('PRIVATE_TOKEN', None)}
        spy_mock.assert_has_calls(calls=[
            call('put', version_controller.get_variable_url("MAJOR"), data={'value': 1}, headers=headers),
            call('put', version_controller.get_variable_url("MINOR"), data={'value': 2}, headers=headers),
            call('put', version_controller.get_variable_url("HOTFIX"), data={'value': 3}, headers=headers)
        ], any_order=True)

        spy_mock.reset_mock()

    @mock.patch('main.VersionController.set_version')
    def test_up_major_when_source_develop_target_release(self,
                                                         set_version: MagicMock):
        version_controller: VersionController = VersionController()
        version_controller.hotfix()
        set_version.assert_called_once_with(major=version_controller._major, minor=version_controller._minor,
                                            hotfix=version_controller._hotfix + 1)

    @mock.patch('main.VersionController.tag_branch')
    @mock.patch('main.VersionController.set_version')
    def test_up_major_when_source_hotfix_target_release(self,
                                                        set_version: MagicMock,
                                                        tag_branch: MagicMock
                                                        ):
        version_controller: VersionController = VersionController()
        version_controller.tag()
        tag_branch.assert_called_once()
        set_version.assert_called_once_with(major=version_controller._major, minor=version_controller._minor,
                                            hotfix=0)
